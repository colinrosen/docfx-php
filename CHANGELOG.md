# Changelog

## Version 1.1.0
 - Add PHP xrefmap.yml file
 - Properly handle union types as separate types

## Version 1.0.1
 - Various bug fixes mostly relating to Union types

## Version 1.0.0

First release

- Namespace parsing
- Class parsing 
  - Constructor
  - Public methods
  - Constants
  - Traits
- Interface parsing
- Enum parsing
- CLI tool
- Code documents in all parsed items
- Support for non-inline @inheritDoc tag
