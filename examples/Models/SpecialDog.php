<?php

namespace com\Picorose\Examples;

class SpecialDog extends Dog
{
    // region Fields

    public static int $totalCount = 0;
    public Human $secondOwner;

    // endregion

    // region Getters

    /**
     * @inheritDoc
     */
    public function getBreed(): string
    {
        // Increment access count
        self::$totalCount++;

        return parent::getBreed();
    }

    // endregion
}