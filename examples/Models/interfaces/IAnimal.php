<?php

namespace com\Picorose\Examples\interfaces;

use com\Picorose\Examples\Color;

/**
 * Data structure for representing some properties of certain animals
 */
interface IAnimal
{
    // region Getters

    /**
     * @return string The name of the animal
     */
    public function getName(): string;

    /**
     * @return Color The main color of the animal
     */
    public function getColor(): Color;

    // endregion

    // region Public

    /**
     * Checks if the animal would eat another animal
     *
     * @param IAnimal $other The animal to be eaten
     * @return bool True if the animal would eat the animal
     */
    public function doesEat(IAnimal $other): bool;

    // endregion
}