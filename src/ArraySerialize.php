<?php
/**
 * Copyright (c) Picorose
 * Licensed under the MIT license. See LICENSE file in the project root for full license information
 *
 * @author Colin Rosen
 * @date 2022
 * @since 1.0.0
 */

namespace com\Picorose\DocFx;

use BackedEnum;
use ReflectionClass;
use UnitEnum;

/**
 * Adds the toArray method to a class
 */
trait ArraySerialize
{
    // region Public

    /**
     * Takes all initialized properties from the object and its parent classes and puts them in an associative array.
     * This array can be used to for example serialize the class into a JSON string. If any of the fields has a type
     * which also has the {@see ArraySerialize::toArray()} method, the field will also be serialized to an array.
     *
     * @return array An array representation of the current object
     */
    public function toArray(): array
    {
        $c = new ReflectionClass($this);

        $arr = [];

        // While the current class has a parent class
        while ($c) {
            foreach ($c->getProperties() as $prop) {
                $var = $prop->isInitialized($this) ? $prop->getValue($this) : null;
                if (empty($var))
                    continue;

                $key = str_replace("_", ".", $prop->getName());

                if (is_array($var)) {
                    $arr2 = $var;
                    for ($i = 0; $i < count($arr2); $i++)
                        if (is_object($arr2[$i]) && method_exists($arr2[$i], "toArray"))
                            $arr2[$i] = $arr2[$i]->toArray();
                    $arr[$key] = $arr2;
                } else if ($var instanceof BackedEnum) {
                    $arr[$key] = $var->value;
                } else if ($var instanceof UnitEnum) {
                    $arr[$key] = $var->name;
                } else if ((is_object($var) || is_string($var)) && method_exists($var, "toArray")) {
                    if (is_string($var))
                        $arr[$key] = $var;
                    else
                        $arr[$key] = $var->toArray();
                } else
                    $arr[$key] = $var;
            }
            $c = $c->getParentClass();
        }

        return $arr;
    }

    // endregion
}