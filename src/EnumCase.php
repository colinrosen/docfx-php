<?php
/**
 * Copyright (c) Picorose
 * Licensed under the MIT license. See LICENSE file in the project root for full license information
 *
 * @author Colin Rosen
 * @date 2022
 * @since 1.0.0
 */

namespace com\Picorose\DocFx;

/**
 * Representation of an single case in an enum object. The `value` field is not shown in the default template of DocFX,
 * but can be used in a custom template to show the backed value of each case.
 */
class EnumCase extends Item
{
    use ArraySerialize;

    // region Fields

    private mixed $value;

    // endregion

    // region Getters

    /**
     * @return mixed The backed value of the current enum case
     */
    public function getValue(): mixed
    {
        return $this->value;
    }

    // endregion

    // region Setters

    /**
     * Sets the current backed value of the current enum case
     *
     * @param mixed $value The new backed value of the current enum case
     */
    public function setValue(mixed $value)
    {
        $this->value = $value;
    }

    // endregion
}