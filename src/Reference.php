<?php
/**
 * Copyright (c) Picorose
 * Licensed under the MIT license. See LICENSE file in the project root for full license information
 *
 * @author Colin Rosen
 * @date 2022
 * @since 1.0.0
 */

namespace com\Picorose\DocFx;

/**
 * An item referenced cross referenced by another item
 * @link https://dotnet.github.io/docfx/tutorial/links_and_cross_references.html
 */
class Reference
{
    use ArraySerialize;

    // region Fields

    private string $uid;
    private string $name;
    private string $href;
    private string $fullName;
    private string $nameWithType;
    private array $spec_php;
    private bool $isExternal;

    // endregion

    // region Setup

    public function __construct(string $uid = "", string $name = "", string $href = "", string $fullName = "", string $nameWithType = "", array $spec = [], bool $isExternal = false)
    {
        $this->uid = $uid;
        $this->name = $name;
        $this->href = $href;
        $this->fullName = $fullName;
        $this->nameWithType = $nameWithType;
        $this->spec_php = $spec;
        $this->isExternal = $isExternal;
    }

    // endregion

    // region Getters

    /**
     * @return string The unique identifier of the reference. Usually the same as the uid of the referenced item.
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @return string The display name of the reference
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string The url pointing to the documentation of this reference. When referring to other items in this
     * project this is left empty
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * @return string The full display name of the item. In programming languages, it's usually the full qualified name.
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @return string The display name of the item including the base type of the item
     */
    public function getNameWithType(): string
    {
        return $this->nameWithType;
    }

    /**
     * @return Reference[] Individual references in this type (for union types)
     */
    public function getSpec(): array
    {
        return $this->spec_php;
    }

    /**
     * @return bool True if the reference references a type outside of this project
     */
    public function isExternal(): bool
    {
        return $this->isExternal;
    }

    // endregion

    // region Setters

    /**
     * @param string $uid The unique identifier of the reference. Usually the same as the uid of the referenced item.
     */
    public function setUid(string $uid)
    {
        $this->uid = $uid;
    }

    /**
     * @param string $name The display name of the reference
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $href The url pointing to the documentation of this reference. When referring to other items in
     * this project this is left empty
     */
    public function setHref(string $href)
    {
        $this->href = $href;
    }

    /**
     * @param string $fullName The full display name of the item. In programming languages, it's usually the full
     * qualified name.
     */
    public function setFullName(string $fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @param string $nameWithType The display name of the item including the base type of the item
     */
    public function setNameWithType(string $nameWithType)
    {
        $this->nameWithType = $nameWithType;
    }

    /**
     * @param array $spec Individual references in this type (for union types)
     */
    public function setSpecs(array $spec)
    {
        $this->spec_php = $spec;
    }

    /**
     * Adds a single reference to this spec in case this reference consists of multiple types
     *
     * @param Reference $spec The reference to add to the spec
     */
    public function addSpec(Reference $spec)
    {
        $this->spec_php[] = $spec;
    }

    /**
     * @param bool $external True if the reference references a type outside of this project
     */
    public function setExternal(bool $external)
    {
        $this->isExternal = $external;
    }

    // endregion
}