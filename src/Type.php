<?php
/**
 * Copyright (c) Picorose
 * Licensed under the MIT license. See LICENSE file in the project root for full license information
 *
 * @author Colin Rosen
 * @date 2022
 * @since 1.0.0
 */

namespace com\Picorose\DocFx;

/**
 * A representation of a class type
 */
class Type
{
    use ArraySerialize;

    // region Fields

    private string $id;
    private string $type;
    private bool $allowsNull;
    private string $description;

    // endregion

    // region Setup

    public function __construct(string $id = "", string $type = "", bool $allowsNull = false, string $description = "")
    {
        $this->id = $id;
        $this->type = $type;
        $this->allowsNull = $allowsNull;
        $this->description = $description;
    }

    // endregion

    // region Getters

    /**
     * @return string The unique id of the type. This is usually the FQDN of the class
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string The FQDN of the class
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return bool True if the type is allowed to have a null value
     */
    public function allowsNull(): bool
    {
        return $this->allowsNull;
    }

    /**
     * @return string The description of the type to display to the user
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    // endregion

    // region Setters

    /**
     * @param string $id The unique id of the type. This is usually the FQDN of the class
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @param string $type The FQDN of the class
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @param bool $allowsNull True if the type is allowed to have a null value
     */
    public function setAllowsNull(bool $allowsNull)
    {
        $this->allowsNull = $allowsNull;
    }

    /**
     * @param string $description The description of the type to display to the user
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    // endregion

    // region Public

    public function __toString(): string
    {
        if (empty($this->id))
            return $this->type;
        if (empty($this->type))
            return "\$$this->id";

        $t = explode("\\", $this->type);
        $t = $t[count($t) - 1];

        return ($this->allowsNull ? "?" : "") . "$t \$$this->id";
    }

    // endregion
}