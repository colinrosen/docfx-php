<?php
/**
 * Copyright (c) Picorose
 * Licensed under the MIT license. See LICENSE file in the project root for full license information
 *
 * @author Colin Rosen
 * @date 2022
 * @since 1.0.0
 */

namespace com\Picorose\DocFx;

/**
 * A representation of the syntax of an item. I.e. it's definition in code, a list of its parameters and its return type.
 */
class Syntax
{
    use ArraySerialize;

    // region Fields

    private string $content;
    private ?Type $return;
    private array $parameters;

    // endregion

    // region Setup

    public function __construct(string $content = "", ?Type $return = null, array $parameters = [])
    {
        $this->content = $content;
        $this->return = $return;
        $this->parameters = $parameters;
    }

    // endregion

    // region Getters

    /**
     * @return string The definition of the item
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return Type|null The return type of the item
     */
    public function getReturn(): ?Type
    {
        return $this->return;
    }

    /**
     * @return Type[] The list of parameters for this item
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    // endregion

    // region Setters

    /**
     * @param string $content The definition of the item
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @param Type|null $return The return type of the item
     */
    public function setReturn(?Type $return)
    {
        $this->return = $return;
    }

    /**
     * @param array $parameters The list of parameters for this item
     */
    public function setParameters(array $parameters): void
    {
        $this->parameters = $parameters;
    }

    /**
     * Adds a single parameter to the syntax
     *
     * @param Type $parameter The parameter to add
     */
    public function addParameter(Type $parameter)
    {
        $this->parameters[] = $parameter;
    }

    // endregion
}