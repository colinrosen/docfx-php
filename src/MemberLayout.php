<?php

namespace com\Picorose\DocFx;

/**
 *  Determines whether type members should be on the same page as containing type or as dedicated pages.
 */
enum MemberLayout: string
{
    /**
     * Place members in the same page as their containing type
     */
    case SAME_PAGE = "SamePage";

    /**
     * Place members in separate pages
     */
    case SEPARATE_PAGES = "SeparatePages";
}
