<?php
/**
 * Copyright (c) Picorose
 * Licensed under the MIT license. See LICENSE file in the project root for full license information
 *
 * @author Colin Rosen
 * @date 2022
 * @since 1.0.0
 */

namespace com\Picorose\DocFx;

/**
 * A single item inside of the {@see TableOfContents}
 */
class TableItem
{
    use ArraySerialize;

    // region Fields

    private string $name;
    private string $href;
    private array $items = [];
    private string $topicUid;
    private string $topicHref;

    // endregion

    // region Getters

    /**
     * @return string The display name of the TOC item
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string The navigation path of the TOC Item. If href is not specified, the TOC Item serves as a container
     * to parent its children TOC Items. If href is an absolute path, the TOC Item navigates to that specified path.
     * If href is a relative path, see below for details.
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * @return TableItem[] The list of tableitems under this item
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return string The uid of the referenced file. If the value is set, it overwrites the value of topicHref.
     */
    public function getTopicUid(): string
    {
        return $this->topicUid;
    }

    /**
     * @return string The topic href of the TOC Item. It is useful when href is linking to a folder or TOC file or
     * tocHref is used.
     */
    public function getTopicHref(): string
    {
        return $this->topicHref;
    }

    // endregion

    // region Setters

    /**
     * @param string $name The display name of the TOC item
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $href The navigation path of the TOC Item. If href is not specified, the TOC Item serves as a
     * container to parent its children TOC Items. If href is an absolute path, the TOC Item navigates to that specified
     * path. If href is a relative path, see below for details.
     */
    public function setHref(string $href)
    {
        $this->href = $href;
    }

    /**
     * @param TableItem[] $items The list of TOC items under this item
     */
    public function setItems(array $items)
    {
        $this->items = $items;
    }

    /**
     * Adds a single TOC item to this item
     *
     * @param TableItem $item The TOC item to add to this item
     */
    public function addItem(TableItem $item)
    {
        $this->items[] = $item;
    }

    /**
     * @param string $topicUid The uid of the referenced file. If the value is set, it overwrites the value of topicHref.
     */
    public function setTopicUid(string $topicUid)
    {
        $this->topicUid = $topicUid;
    }

    /**
     * @param string $topicHref The topic href of the TOC Item. It is useful when href is linking to a folder or TOC
     * file or tocHref is used.
     */
    public function setTopicHref(string $topicHref)
    {
        $this->topicHref = $topicHref;
    }

    // endregion
}