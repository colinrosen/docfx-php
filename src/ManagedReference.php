<?php
/**
 * Copyright (c) Picorose
 * Licensed under the MIT license. See LICENSE file in the project root for full license information
 *
 * @author Colin Rosen
 * @date 2022
 * @since 1.0.0
 */

namespace com\Picorose\DocFx;

use Symfony\Component\Yaml\Yaml;

/**
 * A collection of {@see Item}s and their {@see Reference}s. Typically displayed as a single page in the DocFX
 * documentation.
 */
class ManagedReference implements IYamlObject
{
    use ArraySerialize;

    // region Fields

    /**
     * @var Item[]
     */
    private array $items = [];

    /**
     * @var Reference[]
     */
    private array $references = [];

    private MemberLayout $memberLayout = MemberLayout::SEPARATE_PAGES;

    // endregion

    // region Getters

    /**
     * @return string The unique identifier of the first item in the list. This will always be the main item of this
     * page. E.g. the namespace of the class.
     */
    public function getUid(): string
    {
        if (empty($this->items))
            return "";
        return $this->items[0]->getUid();
    }

    /**
     * @return Item[] The list of items on this page
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return Reference[] The list of referenced items and types on this page
     */
    public function getReferences(): array
    {
        return $this->references;
    }

    /**
     * @return MemberLayout Whether type members should be on the same page as containing type or as dedicated pages.
     */
    public function getMemberLayout(): MemberLayout
    {
        return $this->memberLayout;
    }

    // endregion

    // region Setters

    /**
     * @param Item[] $items The list of items on this page
     */
    public function setItems(array $items)
    {
        $this->items = $items;
    }

    /**
     * Adds a single item to this page
     *
     * @param Item $item The item to add to this page
     */
    public function addItem(Item $item)
    {
        $this->items[] = $item;
    }

    /**
     * @param Reference[] $references The list of referenced items and types on this page
     */
    public function setReferences(array $references)
    {
        $this->references = $references;
    }

    /**
     * Adds a single reference to this page
     *
     * @param Reference $reference A referenced item or type on this page
     */
    public function addReference(Reference $reference)
    {
        $this->references[] = $reference;
    }

    /**
     * Determines whether type members should be on the same page as containing type or as dedicated pages.
     *
     * @param MemberLayout $memberLayout Whether type members should be on the same page as containing type or as dedicated pages.
     */
    public function setMemberLayout(MemberLayout $memberLayout)
    {
        $this->memberLayout = $memberLayout;
    }

    // endregion

    // region Public

    /**
     * @inheritDoc
     */
    public function toYaml(): string
    {
        $yaml = Yaml::dump($this->toArray(), 6, 2);
        return "### YamlMime:ManagedReference\n$yaml";
    }

    // endregion
}